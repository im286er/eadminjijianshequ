<?php
namespace app\index\controller;
use app\common\controller\HomeBase;

use app\common\logic\User as LogicUser;


class User extends  HomeBase
{
	
	// 用户逻辑
	private static $logicUser = null;
	
	
	
	public function _initialize()
	{
		
		parent::_initialize();
		
		self::$logicUser = get_sington_object('logicUser', LogicUser::class);

		$uid=is_login();
		$this->assign('nowuid',$uid);
		if($uid>0){
			$nowuserinfo=self::$datalogic->setname('user')->getDataInfo(['id'=>$uid]);
			$this->assign('nowuserinfo',$nowuserinfo);
		}
		
	}

	public function focususer(){
		
		
		$uid=is_login();
		if($uid==0){
			$this->jump(([RESULT_ERROR, '请登录后操作']));
		}else{
			
			$where['type']=0;
			$where['uid']=$uid;
			$where['sid']=$this->param['useruid'];
			
			if(self::$datalogic->setname('zan')->getStat($where)>0){
				$this->jump(self::$datalogic->setname('zan')->dataDel(['type'=>0,'sid'=>$this->param['useruid'],'uid'=>$uid],'取消关注',true));
				
			}else{
				$data['type']=0;
				$data['sid']=$this->param['useruid'];
				$data['uid']=$uid;
				$this->jump(self::$datalogic->setname('zan')->dataAdd($data,false,'','关注成功'));
			}
			
		
			
			
		}
	
		
		
	}

	public function userfocus(){
		
		!is_login() && $this->jump(RESULT_REDIRECT, '请先登录',es_url('Index/index'));
		
		$uid=is_login();
		empty($this->param['type']) ? $type = 1 : $type = $this->param['type'];
		
		$sidarr=self::$datalogic->setname('zan')->getDataColumn(['uid'=>$uid,'type'=>0],'sid');//得到所有的我关注的人
		$gzidarr=self::$datalogic->setname('zan')->getDataColumn(['sid'=>$uid,'type'=>0],'uid');//得到所有关注我的人
		
		
		
		if($type==1){//好友
			
			if(empty($sidarr)){
				$userlist = array('data'=>array(),'page'=>array());
					
			}else{
				$userlist = self::$datalogic->setname('zan')->getDataList(['m.uid'=>$sidarr,'m.sid'=>$uid,'m.type'=>0],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user|user','user.id=m.uid'],['topic|topic','topic.uid=m.uid','LEFT']],'topic.uid','',false,'m');
					
			}
			
		
		}
		if($type==2){//关注
			if(empty($gzidarr)){
					
				$userlist = self::$datalogic->setname('zan')->getDataList(['m.uid'=>$uid,'m.type'=>0],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user|user','user.id=m.sid'],['topic|topic','topic.uid=m.sid','LEFT']],'topic.uid','',false,'m');
			
					
			}else{
				$userlist = self::$datalogic->setname('zan')->getDataList(['m.uid'=>$uid,'m.type'=>0,'m.sid|!'=>$gzidarr],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user|user','user.id=m.sid'],['topic|topic','topic.uid=m.sid','LEFT']],'topic.uid','',false,'m');
			
			}

		}
		if($type==3){//粉丝
			
			if(empty($sidarr)){
					
				$userlist = self::$datalogic->setname('zan')->getDataList(['m.sid'=>$uid,'m.type'=>0],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user|user','user.id=m.uid'],['topic|topic','topic.uid=m.uid','LEFT']],'topic.uid','',false,'m');
			
					
			}else{
				$userlist = self::$datalogic->setname('zan')->getDataList(['m.sid'=>$uid,'m.type'=>0,'m.uid|!'=>$sidarr],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user|user','user.id=m.uid'],['topic|topic','topic.uid=m.uid','LEFT']],'topic.uid','',false,'m');
			
			}
			
		
		}
		$this->assign('uid',$uid);
		$this->assign('type',$type);
		$this->assign('userlist',$userlist['data']);
		$this->assign('page',$userlist['page']);
		return $this->fetch();
	}

	public function home(){

		
		if(empty($this->param['id'])){
			$this->error('参数错误',es_url('index/index'));
		}else{
			$useruid=$this->param['id'];
			
			//参加的小组
		$joingrouplist = self::$datalogic->setname('user_group')->getDataList('user_group',['m.uid'=>$useruid],'m.group_id,group.name,m.create_time,group.cover_id','m.create_time desc',false,[['group|group','group.id=m.group_id','LEFT']],'','',false,'m');
		$this->assign('joingrouplist',$joingrouplist);
		
		//发表的帖子
		
		$list=self::$datalogic->setname('topic')->getDataList('topic',['m.status'=>1,'m.uid'=>$useruid],'m.*,user.nickname,user.userhead','m.create_time desc',0,[['user|user','user.id=m.uid','LEFT']],'','',false,'m');
		foreach ($list as $k =>$v){
			 
			$comment=$this->request->table('comment')->where(['fid'=>$v['id']])->order('create_time desc')->limit(1)->getList();
			if($comment){
				$list[$k]['ccreate_time']=$comment[0]['create_time'];
				$list[$k]['cuid']=$comment[0]['uid'];
			}
			 
		}
		
		    $this->assign('list',$list);
		
		
			
			$uid=is_login();
			usercz($uid,$useruid,2,3);
			
			$newvisitorlist=self::$datalogic->setname('usercz')->getDataList('usercz',['m.did'=>$useruid,'m.type'=>2,'m.cid'=>3],'m.uid,user.nickname,user.userhead','m.create_time desc',false,[['user','user.id=m.uid','LEFT']],'m.uid',9,false,'m');
			$this->assign('newvisitorlist',$newvisitorlist);
			
			$userinfo=self::$datalogic->setname('user')->getDataInfo(['id'=>$useruid]);
			
			
			$userinfo['topiccount']=self::$datalogic->setname('topic')->getStat(['uid'=>$useruid]);
			
			$userinfo['gzusercount']=self::$datalogic->setname('zan')->getStat(['type'=>0,'sid'=>$useruid]);
			
			$userinfo['fsusercount']=self::$datalogic->setname('zan')->getStat(['uid'=>$useruid,'type'=>0]);
			
			$userinfo['groupcount']=self::$datalogic->setname('user_group')->getStat(['uid'=>$useruid]);
			
			
			$this->assign('userinfo',$userinfo);
			
			$this->assign('useruid',$useruid);
			
		
			if($uid==$useruid){
				$hasfocus=2;
			}else{
				if($this->request->table('zan')->where(['uid'=>$uid,'sid'=>$useruid,'type'=>0])->count()>0){
					$hasfocus=1;
				}else{
					$hasfocus=0;
				}
			}
			$this->assign('hasfocus',$hasfocus);
		}
		
		
			
		return $this->fetch();
			
	}
   public function index(){
   
   	!is_login() && $this->jump(RESULT_REDIRECT, '请先登录',es_url('Index/index'));

   
   
   	return $this->fetch();
   	
   }
   /**
    * 修改个人头像处理
    */
   public function setavatarHandle(){
   	
   	$info=session('member_info');
   	$data=$this->param;
   	$where['id']=$info['id'];
   
   	$obj=new User();
   	$this->jump(self::$datalogic->setname('user')->dataEdit($data,$where,false,'',$info='信息编辑成功',$obj,'callback_setinfo'));
   }
   
   /**
    * 修改个人信息处理
    */
   public function setinfoHandle()
   {
   	
   
   	$info=session('member_info');
   	
   	$data=$this->param;
   	$data['username']=$info['username'];
   	$data['id']=$info['id'];
   	$where['id']=$info['id'];
   	
   	$obj=new User();
   	
   	$this->jump(self::$datalogic->setname('user')->dataEdit($data,$where,true,'',$info='信息编辑成功',$obj,'callback_setinfo'));
   	 
   }
   public function callback_setinfo($result,$data){
   	$member=self::$datalogic->setname('user')->getDataInfo(['id'=>$data['id']]);
   	session('member_info', $member);
   return;
   }
   /**
    * 修改密码处理
    */
   public function setpasswordHandle()
   {
   	$data=$this->param;
   	$mem=session('member_info');
   	$info=self::$datalogic->setname('user')->getDataInfo(['id'=>$mem['id']]);
   	$this->jump(self::$logicUser->setMemberPassword($data, $info));
   	 
   }
   public function mess(){
   	!is_login() && $this->jump(RESULT_REDIRECT, '请先登录',es_url('Index/index'));
   	$uid=is_login();
   	$midarr=self::$datalogic->setname('readmessage')->getDataColumn(['uid'=>$uid],'mid');
   	
   	$where['touid'] = array(0,$uid);
   	$where['status'] = 1;
   	
   	if(!empty($midarr)){
   		
   		$where['id|!'] = $midarr;
   	}
   	
   	$list=self::$datalogic->setname('message')->getDataList($where,true,'update_time desc');
   	
   	$this->assign('list',$list['data']);
   	$this->assign('page',$list['page']);
   	return $this->fetch();
   
   }
public function ajaxdelmess(){
	$myuid=is_login();
	$id=$this->param['id'];
	$uid=$this->param['uid'];
	if($uid>0){
		
		$where['id']=$id;
		
		$this->jump(self::$datalogic->setname('message')->dataDel($where,'删除成功',true));
	}else{
		$data['uid']=$myuid;
		$data['mid']=$id;
		$this->jump(self::$datalogic->setname('readmessage')->dataAdd($data,false,'','删除成功'));
	}
	
}
public function ajaxdelallmess(){
	$uid=is_login();
	$midarr=self::$datalogic->setname('readmessage')->getDataColumn(['uid'=>$uid],'mid');
	$where['touid']=$uid;
	if(!empty($midarr)){
		$where['id|!']=$midarr;
	}
	
	self::$datalogic->setname('message')->dataDel($where,'',true);//删除私信
	
	$where1['touid']=0;
	if(!empty($midarr)){
		$where1['id|!']=$midarr;
	}
	$list=self::$datalogic->setname('message')->getDataList($where1,true,'update_time desc',false);
	
	foreach ($list as $k =>$v){
		$data['uid']=$uid;
		$data['mid']=$v['id'];
		$n=self::$datalogic->setname('readmessage')->dataAdd($data,false,'','删除成功');
		
		
	}
	
	$this->jump([RESULT_SUCCESS, '清空成功']);
	
}



   public function shoucang(){
   	!is_login() && $this->jump(RESULT_REDIRECT, '请先登录',es_url('Index/index'));
   	$uid=is_login();
 
   	empty($this->param['type']) ? $type = 1 : $type = $this->param['type'];//1表示帖子
   	if($type==1){
   		$topiclist = self::$datalogic->setname('zan')->getDataList(['m.uid'=>$uid,'m.type'=>3],'m.*,topic.title,topic.choice,topic.settop,topic.reply,topic.view,topic.create_time as topiccreate_time,user.nickname,user.userhead,group.name','m.create_time desc',8,[['topic|topic','topic.id=m.sid'],['group|group','group.id=topic.tid'],['user|user','user.id=topic.uid']],'','',false,'m');
   		
   	}else{

   		
   		
   	}
   	$this->assign('type',$type);
   	$this->assign('topiclist',$topiclist['data']);
   	$this->assign('page',$topiclist['page']);
   	return $this->fetch();
   	 
   }
   public function mygroup(){
   	!is_login() && $this->jump(RESULT_REDIRECT, '请先登录',es_url('Index/index'));
   	$uid=is_login();
   
   	empty($this->param['type']) ? $type = 1 : $type = $this->param['type'];//1表示建立的2表示关注的
   	if($type==1){
   		$grouplist = self::$datalogic->setname('user_group')->getDataList(['m.uid'=>$uid,'m.grade'=>2],'m.*,group.cover_id,group.membercount,group.topiccount,group.name','m.create_time desc',8,[['group','group.id=m.group_id']],'','',false,'m');
   		 
   	}else{
   
   		$grouplist = self::$datalogic->setname('user_group')->getDataList(['m.uid'=>$uid,'m.grade'=>array('neq',2)],'m.*,group.cover_id,group.membercount,group.topiccount,group.name','m.create_time desc',8,[['group','group.id=m.group_id']],'','',false,'m');
   		 
   	}
   	$this->assign('type',$type);
   	$this->assign('grouplist',$grouplist['data']);
   	$this->assign('grouplist',$grouplist['page']);
   	return $this->fetch();
   	 
   }
   public function mytopic(){
   	!is_login() && $this->jump(RESULT_REDIRECT, '请先登录',es_url('Index/index'));
   	$uid=is_login();
 
   
   
   		$topiclist = self::$datalogic->setname('topic')->getDataList(['m.uid'=>$uid,'m.status'=>1],'m.*,user.nickname,user.userhead,group.name','m.create_time desc',8,[['group|group','group.id=m.tid'],['user|user','user.id=m.uid']],'','',false,'m');
   		
  
   
   	$this->assign('topiclist',$topiclist['data']);
   	$this->assign('page',$topiclist['page']);
   	return $this->fetch();
   	 
   }
   /**
    * 忘记密码页面
    */
   public function forget(){
   	session('http_referer',1);
   	if (IS_POST) {
   	
   	
   		$datan=$this->request->param();

   		$n=self::$datalogic->setname('user')->getDataInfo(['usermail'=>$datan['email']]);
   		
   		
   	
   		 
   		if(empty($n)||($n['status']!=2&&$n['status']!=5)){
   			$this->error(0, '', array('code' => 0, 'msg' => '邮箱未激活或邮箱未注册'));
   		}else{
   	
   			$data['email']=$n['usermail'];
   	
   			$data['title']='找回密码';
   			$str=md5($n['salt'].$n['id'].$n['usermail']);
   	
   			$data['body']='http://'.$_SERVER['HTTP_HOST'].es_url('user/resetmima',array('mod'=>$n['id'],'id'=>$str));
   	
   	
   			asyn_sendmail($data);
   			
   			$this->success(200, '', array('code'=>200,'msg'=>'邮件已发送，请到邮箱进行查收'));
   				
   				
   		}
   	
   	
   	
   	
   	}else{
   	
   		 
   	}

   	return $this->fetch();
   	 
   }
   
   public function resetmima(){
   	 
   	$data=$this->request->param();
   	$n=self::$datalogic->setname('user')->getDataInfo(['id'=>$data['mod']]);
   	
   	if(md5($n['salt'].$n['id'].$n['usermail'])==$data['id']){
   	
   		$this->assign('userid',$n['id']);
   		$this->assign('salt',md5($n['salt']));
   		$this->assign('username',$n['username']);
   		
   		return $this->fetch();
   	}else{
   		$this->error('非法操作',es_url('user/forget'));
   	}
   	 
   }
   public function resetpass()
   {
   	$data=$this->request->param();
   	$n=self::$datalogic->setname('user')->getDataInfo(['id'=>$data['uid']]);
   	if(md5($n['salt'])==$data['salt']){
   		
   		if(md5($data['password'].$n['salt'])==$n['password']){
   			
   			$this->jump([RESULT_SUCCESS, '密码重置成功']);
   			
   		}else{
   			
   			$m['password']= md5($data['password'].$n['salt']);
   			 
   			$this->jump(self::$datalogic->setname('user')->dataEdit($m,['id'=>$n['id']],false,'密码重置成功'));
   		}
   		
   		
   	
   	}else{
   		$this->error('非法操作',es_url('index/index'));
   	}
   	
   	
   
   	 
   }
   /**
    * 注册页面
    */
   public function register(){

   	is_login() && $this->jump(RESULT_REDIRECT,'',es_url('Index/index'));
   	
   	$yzm_list = parse_config_array('yzm_list');//1\注册2\登录3\忘记密码4\后台登录
   	 
   	if(in_array(1, $yzm_list)){
   		 
   		$yzm=1;
   		 
   	}else{
   		 
   		$yzm=0;
   		 
   	}
   	
   	$this->assign('yzm',$yzm);
   	
   	return $this->fetch();
   	 
   }
   /**
    * 注册处理
    */
   public function regHandle($username = '', $password = '', $repassword = '',$usermail = '', $verify = '')
   {
   	 
   	$this->jump(self::$logicUser->regHandle($username, $password, $repassword,$usermail, $verify));
   
   }   
   
   
   
   
   /**
    * 登录页面
    */
   public function login(){
   	
   	is_login() && $this->jump(RESULT_REDIRECT, '',es_url('Index/index'));
   	
   	$yzm_list = parse_config_array('yzm_list');//1\注册2\登录3\忘记密码4\后台登录
   	
   	if(in_array(2, $yzm_list)){
   		
   		$yzm=1;
   		
   	}else{
   		
   		$yzm=0;
   		
   	}
   	
   	$this->assign('yzm',$yzm);
   	
   	return $this->fetch();
   	 
   }
   /**
    * 登录处理
    */
   public function loginHandle($username = '', $password = '', $verify = '')
   {
   	 
   	$this->jump(self::$logicUser->loginHandle($username, $password, $verify));
   	
   }
   /**
    * 注销处理
    */
   public function logout()
   {
   	 
   	$this->jump(self::$logicUser->logout());
   
   }
   
}
