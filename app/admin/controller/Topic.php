<?php

namespace app\admin\controller;
use app\common\logic\Common as LogicCommon;

/**
 * 文档控制器
 */
class Topic extends AdminBase
{
    
	private static $commonLogic = null;
    /**
     * 构造方法
     */
    public function _initialize()
    {
        
        parent::_initialize();
        self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class,'topic');
   }

    /**
     * 获取列表搜索条件
     */
    public function getWhere($data = [])
    {
    
    	$where = [];
    
    	$where['m.status']=array('egt',0);
    
    	if (!is_administrator()) {
    
    		 
    	}
    
    	return $where;
    }
    /**
     * 文档列表
     */
    public function topicList()
    {
        
        $where = $this->getWhere($this->param);
        
        $clist = self::$commonLogic->getDataList($where, 'm.*,user.nickname,groupcate.name as gidname,group.name as tidname', 'm.id desc',0,[['user|user','user.id=m.uid'],['groupcate|groupcate','groupcate.id=m.gid'],['group|group','group.id=m.tid']]);
        
        $this->assign('list', $clist['data']);
       
        $this->assign('page', $clist['page']);
       
        return $this->fetch('topic_list');
    }
    
    /**
     * 文档添加
     */
    public function topicAdd()
    {
        
        IS_POST && $this->jump(self::$commonLogic->dataAdd($this->param));
        
        return $this->fetch('topic_add');
    }
    /**
     * 文档编辑
     */
    public function topicEdit()
    {
    	
    	IS_POST && $this->jump(self::$commonLogic->dataEdit($this->param,['id'=>$this->param['id']]));
    	$info = self::$commonLogic->getDataInfo(['id' => $this->param['id']]);
    	
    	$this->assign('info', $info);
    	return $this->fetch('topic_edit');
    }
    /**
     * 文档批量删除
     */
    public function topicAlldel($ids = 0)
    {
    
    	$this->jump(self::$commonLogic->dataDel(['id' => $ids],'删除成功',true));
    }
    /**
     * 文档删除
     */
    public function topicDel($id = 0)
    {
        
        $this->jump(self::$commonLogic->dataDel(['id' => $id],'删除成功',true));
    }
    /**
     * 文档状态更新
     */
    public function topicCstatus($id = 0,$status,$field)
    {
    	
        $this->jump(self::$commonLogic->setDataValue(['id' => $id],$field,$status));
    }
    /**
     * 文档审核
     */
    public function topicSh($id = 0,$status,$field)
    {
    
    	$this->jump(self::$commonLogic->setDataValue(['id' => $id],$field,$status));
    }
    
    /**
     * 文档批量审核
     */
    public function topicAllSh($ids = 0)
    {
    
    	$this->jump(self::$commonLogic->setDataValue(['id'=>$ids],$field,$status));
    }
    

}
